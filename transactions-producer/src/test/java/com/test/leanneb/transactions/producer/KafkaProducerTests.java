package com.test.leanneb.transactions.producer;

import com.test.leanneb.transactions.producer.kafka.KafkaProducer;
import com.test.leanneb.transactions.producer.pojos.Transaction;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.hamcrest.KafkaMatchers;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@SpringBootTest(classes = { KafkaAutoConfiguration.class, KafkaProducerTests.KafkaProducerTestConfiguration.class })
public class KafkaProducerTests {

	@Configuration
	static class KafkaProducerTestConfiguration {

		@Bean
		public KafkaProducer kafkaProducer() {
			return new KafkaProducer();
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerTests.class);

	private static final String TOPIC = "producer.t";

	private BlockingDeque<ConsumerRecord<Long, Transaction>> records;

	private KafkaMessageListenerContainer<Long, Transaction> container;

	@Autowired
	private KafkaProducer kafkaProducer;

	@ClassRule
	public static KafkaEmbedded kafkaEmbedded = new KafkaEmbedded(1, true, TOPIC);

	public KafkaProducerTests(){}

	@Before
	public void init() throws Exception {

		// Setup the consumer properties
		Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps("producer", "false", kafkaEmbedded);

		// Create the kafka consumer factory
		DefaultKafkaConsumerFactory<Long, Transaction> consumerFactory = new DefaultKafkaConsumerFactory<Long, Transaction>(consumerProperties,
				new LongDeserializer(), new JsonDeserializer<>(Transaction.class));

		// Setup the topic that needs to be consumed
		ContainerProperties containerProperties = new ContainerProperties(TOPIC);

		// Create a kafka message listener container
		this.container = new KafkaMessageListenerContainer<Long, Transaction>(consumerFactory, containerProperties);

		// Create a thread safe queue to store the records
		this.records = new LinkedBlockingDeque<>();

		// Setup the kafka message listener
		this.container.setupMessageListener(new MessageListener<Long, Transaction>() {
			@Override
			public void onMessage(ConsumerRecord<Long, Transaction> record) {
				LOGGER.debug("Received message {} with key {}.", record.value().toString(), record.key());
				records.add(record);
			}
		});

		// Start the container and the underlying message listener
		this.container.start();

		// Wait until the container has the required number of assigned partitions
		ContainerTestUtils.waitForAssignment(container, kafkaEmbedded.getPartitionsPerTopic());
	}

	@After
	public void tearDown(){
		this.container.stop();
	}

	@Test
	public void testSend() throws InterruptedException {
		Transaction transaction = new Transaction(1, 1, 10,25, "a");
		this.kafkaProducer.send(transaction);

		ConsumerRecord<Long, Transaction> receivedMsg = this.records.poll(3, TimeUnit.SECONDS);

		Assert.assertEquals(receivedMsg.value(), transaction);
		Assert.assertThat(receivedMsg, KafkaMatchers.hasKey(transaction.getId()));
	}

}
