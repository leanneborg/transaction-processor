package com.test.leanneb.transactions.producer;

import com.test.leanneb.transactions.producer.kafka.KafkaProducer;
import com.test.leanneb.transactions.producer.pojos.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;

/*
 * This class is responsible for reading the transactions from file and sending each one
 * over a kafka message queue.
 */
@Component
public class TransactionsProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsProcessor.class);

    @Value("${transactions.path}")
    private String transactionsFilePath;

    @Autowired
    private KafkaProducer kafkaProducer;

    /*
     * The reader responsible for reading the transactions from file
     */
    private BufferedReader transactionsStream;

    @PostConstruct
    public void init() {
        this.transactionsStream = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(this.transactionsFilePath)));
        this.transactionsStream.lines().forEach(line -> {

            String[] splitLine = line.split(" ");
            this.kafkaProducer.send(new Transaction(Long.parseLong(splitLine[0]), Long.parseLong(splitLine[1]),
                    Integer.parseInt(splitLine[2]), Integer.parseInt(splitLine[3]), splitLine[4]));
        });
    }

    @PreDestroy
    public void destroy() {
        try {
            this.transactionsStream.close();
        } catch (IOException e) {
            LOGGER.error("Failed to close input stream.", e);
        }
    }
}
