package com.test.leanneb.transactions.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.test.leanneb.transactions.producer" })
public class TransactionsProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionsProducerApplication.class, args);
	}
}