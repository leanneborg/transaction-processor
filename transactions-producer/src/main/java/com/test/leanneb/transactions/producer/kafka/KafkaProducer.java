package com.test.leanneb.transactions.producer.kafka;

import com.test.leanneb.transactions.producer.pojos.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Value("${spring.kafka.template.default-topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<Long, Transaction> kafkaTemplate;

    /**
     * Sends the provided transaction on the message queue.
     * @param transaction - The transaction to be sent over the queue.
     */
    public void send(Transaction transaction) {
        LOGGER.debug("Sending transaction {}", transaction, transaction.getUserId());
        this.kafkaTemplate.send(this.topic, transaction.getId(), transaction);
    }
}
