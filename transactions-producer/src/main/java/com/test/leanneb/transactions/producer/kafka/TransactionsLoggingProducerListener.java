package com.test.leanneb.transactions.producer.kafka;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.LoggingProducerListener;
import org.springframework.stereotype.Component;

/**
 * This class extends the LoggingProducerListener, which responsible for logging any message sending
 * failures, by logging even successfully sent messages.
 */
@Component
public class TransactionsLoggingProducerListener extends LoggingProducerListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionsLoggingProducerListener.class);


    @Override
    public void onSuccess(String topic, Integer partition, Object key, Object value, RecordMetadata recordMetadata) {
        LOGGER.info("Sent message {} with key {} on topic {}.", value, key, topic);
    }

    @Override
    public boolean isInterestedInSuccess() {
        return true;
    }
}
