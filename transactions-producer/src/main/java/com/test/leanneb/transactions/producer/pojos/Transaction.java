package com.test.leanneb.transactions.producer.pojos;

/*
 * This class holds the information of a single transaction.
 */
public class Transaction {

    /*
     * The transaction ID
     */
    private long id;

    /*
     * The user ID
     */
    private long userId;

    /*
     * The bet in cents
     */
    private int betInCents;

    /*
     * The amount won in cents
     */
    private int winInCents;

    /*
     * The session ID
     */
    private String sessionId;


    /**
     * Default constructor
     */
    public Transaction() { }

    /**
     * Constructor
     */
    public Transaction(long id, long userId, int betInCents, int winInCents, String sessionId) {
        this.id = id;
        this.userId = userId;
        this.betInCents = betInCents;
        this.winInCents = winInCents;
        this.sessionId = sessionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getBetInCents() {
        return betInCents;
    }

    public void setBetInCents(int betInCents) {
        this.betInCents = betInCents;
    }

    public int getWinInCents() {
        return winInCents;
    }

    public void setWinInCents(int winInCents) {
        this.winInCents = winInCents;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.id).
                append("\tUser ID: ").append(this.userId).
                append("\tSession ID: ").append(this.sessionId).
                append("\tBet: ").append(this.betInCents).
                append("\tWin: ").append(this.winInCents);
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {

        if(this == null && obj != null){
            return false;
        }

        if(this != null && obj == null){
            return false;
        }

        if(!obj.getClass().equals(Transaction.class)) {
            return false;
        }

        Transaction transaction = (Transaction)obj;

        if(this.id != transaction.getId()) {
            return false;
        }

        if(this.userId != transaction.getUserId()) {
            return false;
        }

        if(this.betInCents != transaction.getBetInCents()) {
            return false;
        }

        if(this.winInCents != transaction.getWinInCents()) {
            return false;
        }

        if(this.sessionId == null && transaction.getSessionId() == null) {
            return true;
        }

        if(this.sessionId == null && transaction.getSessionId() != null) {
            return false;
        }

        if(this.sessionId != null && transaction.getSessionId() == null) {
            return false;
        }

        if(!this.sessionId.equals(transaction.getSessionId())) {
            return false;
        }

        return true;
    }
}
