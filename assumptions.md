# Assumptions

During the development of this project, the following assumptions were made:

* No duplicate messages would be attempted for subscription by the server. This means that if the server is run twice the same transactions would be published on the queue; and as a result the same transaction processed twice by the consumer.
* Only one consumer would be run; although multiple consumers can be easily started by changing the `spring.kafka.consumer.group-id` property within *application.yml* found in the */src/main/resources* of the ***transactions-consumer*** component. Please note that each consumer requires a different group ID.
* If multiple consumers are run, it is assumed that all of them would be interested in all the messages. This means that the consumers are not assumed to be in a cluster setup, as a result of which messages might require to be processed by only one of the consumers.