transaction-processor
=====================

This project is composed of two sub-projects, namely:

* transaction-producer - This is responsible of reading a number of transactions from a file and sending each one of them on a message queue.
* transaction-consumer - This is responsible of consuming the transactions on the message queue and outputting a summary of these transactions.

### Prerequisites ###

In order to install and run this project, one must ensure that the following prerequisites are satisfied:

* Java 8
* Maven
* Zookeeper
* Kafka

In case of Mac OS, these can be easily installed using *brew*.

### Building the Project ###

The project can be built using the following command:

```
mvn clean install

```

Tests can be run using:

```
mvn clean test
```

### Setup ###

Prior to the execution of the components within this project, the following steps need to be taken:

* Change your current directory to kafka home.
* Start zookeeper by executing command `zookeeper-server-start zookeeper.properties`
* Start kafka server by executing command `kafka-server-start server.properties`

The producer can then be started by running the following command within the target folder to the ***transactions-producer*** project:
```
java -jar transactions-producer-0.0.1-SNAPSHOT.jar
```
When running the producer, one will see the list of transactions sent using Kafka. The transactions-consumer, which is responsible for consuming such messages,
can be run be executing the following command within the target folder of the ***transactions-consumer*** project:
```
java -jar transactions-consumer-0.0.1-SNAPSHOT.jar
```

After the consumption of each message, the consumer outputs a summary of the data for the user and session within the consumed message, whereby the summary
will give a summation of the bets and money won by the user in that particular session.