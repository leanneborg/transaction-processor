package com.test.leanneb.transactions.consumer;

import com.test.leanneb.transactions.consumer.configurations.KafkaConsumerConfiguration;
import com.test.leanneb.transactions.consumer.kafka.KafkaConsumer;
import com.test.leanneb.transactions.consumer.pojos.Transaction;
import org.apache.kafka.common.serialization.LongSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.swing.tree.TreeNode;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { KafkaConsumerConfiguration.class, KafkaConsumerTests.KafkaConsumerTestConfiguration.class })
@DirtiesContext
public class KafkaConsumerTests {

	@Configuration
	static class KafkaConsumerTestConfiguration{

		@Bean
		public KafkaConsumer kafkaConsumer() {
			return new KafkaConsumer();
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerTests.class);

	private static final String TOPIC = "consumer.t";

	@Autowired
	private KafkaConsumer kafkaConsumer;

	@Autowired
	private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

	private KafkaTemplate<Long, Transaction> kafkaTemplate;

	@ClassRule
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded(1, true, TOPIC);

	public KafkaConsumerTests() {}

	@Before
	public void init() throws Exception {

		// Set the kafka producer properties
		Map<String, Object> senderProps = KafkaTestUtils.senderProps(embeddedKafka.getBrokersAsString());

		// Create a Kafka producer factory
		ProducerFactory<Long, Transaction> producerFactory = new DefaultKafkaProducerFactory<>(senderProps, new LongSerializer(), new JsonSerializer<>());

		// Create the kafka template
		this.kafkaTemplate = new KafkaTemplate<>(producerFactory);

		// Set the default topic to send to
		this.kafkaTemplate.setDefaultTopic(TOPIC);

		// Wait until partitions are assigned
		for (MessageListenerContainer messageListenerContainer : this.kafkaListenerEndpointRegistry.getListenerContainers()) {
			ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafka.getPartitionsPerTopic());
		}
	}


	@Test
	public void testReceive() throws InterruptedException {

		Transaction transaction1 = new Transaction(3, 1, 76,12,"c");
		this.kafkaTemplate.send(TOPIC, transaction1.getId(), transaction1);
		LOGGER.debug("Sent message: {}", transaction1.toString());

		Transaction transaction2 = new Transaction(4, 1, 0,8,"c");
		this.kafkaTemplate.send(TOPIC, transaction2.getId(), transaction2);
		LOGGER.debug("Sent message: {}", transaction2.toString());

		Transaction transaction3 = new Transaction(4, 1, 10,0,"a");
		this.kafkaTemplate.send(TOPIC, transaction3.getId(), transaction3);
		LOGGER.debug("Sent message: {}", transaction3.toString());

		// Wait for the messages to be consumed
		Thread.sleep(3000);

		ConcurrentMap<String, MessageProcessor> messageProcessors = this.kafkaConsumer.getMessageProcessors();
		Assert.assertEquals(2, messageProcessors.keySet().size());

		Assert.assertEquals(transaction1.getBetInCents() + transaction2.getBetInCents(),
				messageProcessors.get(String.format("%d-%s", transaction1.getUserId(), transaction1.getSessionId())).getBetInCents());

		Assert.assertEquals(transaction1.getWinInCents() + transaction2.getWinInCents(),
				messageProcessors.get(String.format("%d-%s", transaction1.getUserId(), transaction1.getSessionId())).getWinInCents());

		Assert.assertEquals(transaction3.getBetInCents(),
				messageProcessors.get(String.format("%d-%s", transaction3.getUserId(), transaction3.getSessionId())).getBetInCents());

		Assert.assertEquals(transaction3.getWinInCents(),
				messageProcessors.get(String.format("%d-%s", transaction3.getUserId(), transaction3.getSessionId())).getWinInCents());

	}
}
