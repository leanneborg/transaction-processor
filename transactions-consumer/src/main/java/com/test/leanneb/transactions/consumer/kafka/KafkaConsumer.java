package com.test.leanneb.transactions.consumer.kafka;

import com.test.leanneb.transactions.consumer.MessageProcessor;
import com.test.leanneb.transactions.consumer.pojos.Transaction;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.*;

@Component
public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    private ConcurrentMap<String, MessageProcessor> messageProcessors = new ConcurrentHashMap<>();

    @Value("${consumer.thread.pool.size:3}")
    private int threadPoolExecutorSize;

    private ExecutorService executorService;


    @PostConstruct
    public void init() {
        this.executorService = Executors.newFixedThreadPool(this.threadPoolExecutorSize);
    }

    @KafkaListener(topics = { "${spring.kafka.template.default-topic}" })
    public void receive(ConsumerRecord<Long, Transaction> consumerRecord) {

        CompletableFuture<MessageProcessor> promise = CompletableFuture.supplyAsync(() -> {

            MessageProcessor msgProc = this.messageProcessors.getOrDefault(MessageProcessor.GenerateKey(consumerRecord.value().getUserId(),
                    consumerRecord.value().getSessionId()), null);

            if(msgProc == null) {
                msgProc = new MessageProcessor(consumerRecord.value());
                this.messageProcessors.putIfAbsent(msgProc.getKey(), msgProc);
            } else {
                msgProc.addToSummary(consumerRecord.value());
            }

            return msgProc;
        });

        promise.thenAccept(msgProc -> {
           System.out.println(msgProc.toString());
        });

        this.executorService.submit(() -> {
            try {
                promise.get(100, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                LOGGER.error("Failed to process transaction.", e);
            }
        });
    }

    public ConcurrentMap<String, MessageProcessor> getMessageProcessors() {
        return messageProcessors;
    }

    @PreDestroy
    public void destroy() {
        this.executorService.shutdown();
    }

}
