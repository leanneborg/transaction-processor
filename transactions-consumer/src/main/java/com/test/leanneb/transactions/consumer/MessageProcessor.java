package com.test.leanneb.transactions.consumer;

import com.test.leanneb.transactions.consumer.pojos.Transaction;
import org.springframework.context.annotation.ComponentScan;

import java.text.DecimalFormat;
import java.util.concurrent.locks.ReentrantLock;

public class MessageProcessor {

    private Long userId;

    private String sessionId;

    private int betInCents;

    private int winInCents;

    private ReentrantLock lock = new ReentrantLock();

    private DecimalFormat format = new DecimalFormat("0.00");


    public MessageProcessor(Transaction transaction) {

        this.userId = transaction.getUserId();
        this.sessionId = transaction.getSessionId();
        this.betInCents = transaction.getBetInCents();
        this.winInCents = transaction.getWinInCents();
    }

    public int getBetInCents() {
        return betInCents;
    }

    public int getWinInCents() {
        return winInCents;
    }

    public void addToSummary(Transaction transaction) {
        try {
            this.lock.lock();

            this.betInCents += transaction.getBetInCents();
            this.winInCents += transaction.getWinInCents();
        } finally {
            this.lock.unlock();
        }
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("User ID: ").append(this.userId).
                append("\tSession ID: ").append(this.sessionId).
                append("\tBet: ").append(this.format.format(this.betInCents/100.0)).append("€").
                append("\tWin: ").append(this.format.format(this.winInCents/100.0)).append("€");
        return sb.toString();
    }

    public String getKey(){
        return GenerateKey(this.userId, this.sessionId);
    }

    public static String GenerateKey(Long userId, String sessionId) {
        return String.format("%d-%s", userId, sessionId);
    }
}
