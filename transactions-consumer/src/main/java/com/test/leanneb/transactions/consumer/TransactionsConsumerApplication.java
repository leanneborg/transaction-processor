package com.test.leanneb.transactions.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.test.leanneb.transactions.consumer" })
public class TransactionsConsumerApplication {

	public static void main(String[] args) {

		SpringApplication.run(TransactionsConsumerApplication.class, args);
	}
}
